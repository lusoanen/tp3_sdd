#ifndef _ARBRE_H_
#define _ARBRE_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "bool.h"
#include "pile.h"
#include "noeud.h"

typedef struct arbre
{
    noeud_t *racine; 
} arbre_t;


noeud_t * creerNoeud(noeud_t **, char);
noeud_t * creerNoeud_v2(char);
void libererArbre(noeud_t*);
void libererNoeud(noeud_t*);
void afficherArbreDebug(noeud_t *);


#endif