#ifndef _ASSISTANT_H_
#define _ASSISTANT_H_

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "arbre.h"
#include "gestionArbre.h"

#define TAILLEMAX 128

noeud_t ** rechercheInsertion();
noeud_t ** rechercheInsertionFrere(noeud_t ** debut, char lettre);
void motVersBonFormat(char * mot, int taille);

#endif