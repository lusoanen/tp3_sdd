#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "menu.h"
#include "arbre.h"
#include "pile.h"
#include "expressionAlgebrique.h"
#include "gestionArbre.h"

#define TAILLEMAX 128

/*****************************************************/
/* main()                                            */
/*                                                   */
/* point d'entrée du programme                       */
/*                                                   */
/* Input :                                           */
/* Output :                                          */
/*****************************************************/

int main(){
    int choix = 1;

    arbre_t arbre = {NULL};
    pile_t *pileArbre = creerPile();
    char expAlg[] = EA;
    char motif[TAILLEMAX];
    char mot[TAILLEMAX];

    arbre.racine = construireArbre(arbre.racine, expAlg, *pileArbre);

    while (choix != 0)
    {
        afficherMenu();
        scanf("%d%*c", &choix);

        switch (choix)
        {
        case 1:
            printf("\nVotre arbre actuel :\n");
            afficherArbreDebug(arbre.racine);
            printf("\nIl contient les mots suivants\n");
            afficherMots(arbre.racine, NULL);
            printf("\nTaper entrée pour continuer\n");
            getchar();
            break;
        case 2:
            printf("\nQuel mot souhaitez-vous insérer ?\n");
            fgets(mot, TAILLEMAX, stdin);
            mot[strlen(mot)-1] = '\0';
            printf("\n");
            insererMot(&arbre, mot);
            printf("\nTaper entrée pour continuer\n");
            getchar();
            break;
        case 3:
            printf("\nQuel motif souhaitez-vous rechercher ?\n");
            fgets(motif, TAILLEMAX, stdin);
            motif[strlen(motif)-1] = '\0';
            printf("\n");
            rechercheMotif(motif, arbre);
            printf("\nTaper entrée pour continuer\n");
            getchar();
            break;
        default:
            break;
        }
    }

    libererPile(pileArbre);
    libererArbre(arbre.racine);

    return 1;
}