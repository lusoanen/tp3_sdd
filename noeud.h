#ifndef _NOEUD_H_
#define _NOEUD_H_


typedef struct noeud
{
    char token;          
    struct noeud *fils;
    struct noeud *frere;
} noeud_t;


#endif