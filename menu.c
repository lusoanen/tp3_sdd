#include "menu.h"


/*****************************************************/
/* afficherMenu()                                    */
/*                                                   */
/* Affiche le menu pricipale                         */
/*                                                   */
/* Input :                                           */
/* Output :                                          */
/*****************************************************/

void afficherMenu()
{
	system("clear");
	printf("Bienvenue dans le menu principal. Veuillez selectionner une option :\n");
	printf("\t-------------------------------------------------\n");
	printf("\t| 1 - Affichage du dictionnaire                 |\n");
	printf("\t| 2 - Inserer un nouveau mot                    |\n");
	printf("\t| 3 - Rechercher d'un motif                     |\n");
    printf("\t| 0 - Sortir                                    |\n");
	printf("\t-------------------------------------------------\n");
}