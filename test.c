#include "menu.h"
#include "expressionAlgebrique.h"
#include "gestionArbre.h"
#include "pile.h"
#include <string.h>
#define TAILLE  100


void testConstructionArbre(arbre_t * arbre){

    pile_t *pileArbre = creerPile();
    char expAlg[] = EA;

    if(expAlg != NULL){
        arbre->racine = construireArbre(arbre->racine, expAlg, *pileArbre);
    }
    else
        printf("erreur ea\n");
    libererPile(pileArbre);

}

void testAffichage(arbre_t * arbre){

    /*

    arbre_t * arbre = malloc(sizeof(arbre_t));
    arbre->racine = NULL; 

    noeud_t * res0 = malloc(sizeof(noeud_t));
    res0->token = 'a';
    res0->frere = NULL;
    res0->fils = NULL;
    arbre->racine = res0;

    noeud_t * res1 = malloc(sizeof(noeud_t));
    res1->token = 'r';
    res1->frere = NULL;
    res1->fils = NULL;
    arbre->racine->fils = res1;

    noeud_t * res2 = malloc(sizeof(noeud_t));
    res2->token = 'T';
    res2->frere = NULL;
    res2->fils = NULL;
    arbre->racine->fils->fils = res2;

    noeud_t * res3 = malloc(sizeof(noeud_t));
    res3->token = 'A';
    res3->frere = NULL;
    res3->fils = NULL;
    arbre->racine->fils->fils->frere = res3;

    noeud_t * res4 = malloc(sizeof(noeud_t));
    res4->token = 'b';
    res4->frere = NULL;
    res4->fils = NULL;
    arbre->racine->frere = res4;

    noeud_t * res5 = malloc(sizeof(noeud_t));
    res5->token = 'O';
    res5->frere = NULL;
    res5->fils = NULL;
    arbre->racine->frere->fils = res5;

    */

    printf("\n");
    afficherMots(arbre->racine,NULL);
    printf("\n");
}

void testInsertion(arbre_t * arbre)
{
    char * mot1 = "Aba";
    char * mot2 = "Abattu";
    char * mot3 = "Patate";
    char * mot4 = "Artichaut";
    char * mot5 = "Artiste";

    insererMot(arbre,mot1);
    insererMot(arbre,mot2);
    insererMot(arbre,mot3);
    insererMot(arbre,mot4);
    insererMot(arbre,mot5);
}

void testRecherche(arbre_t arbre){

    char motifATester[] = "aba";
    rechercheMotif(motifATester, arbre);

    char motifATester2[] = "a";
    rechercheMotif(motifATester2, arbre);

    char motifATester3[] = "ab";
    rechercheMotif(motifATester3, arbre);

    char motifATester4[] = "";
    rechercheMotif(motifATester4, arbre);

    char motifATester5[] = "balE";
    rechercheMotif(motifATester5, arbre);

    char motifATester6[] = "ACD";
    rechercheMotif(motifATester6, arbre);
    
}

int main(){

    arbre_t arbre = {NULL};
    testConstructionArbre(&arbre);
    afficherArbreDebug(arbre.racine);
    testAffichage(&arbre);
    testRecherche(arbre);
    testInsertion(&arbre);
    testAffichage(&arbre);
    afficherArbreDebug(arbre.racine);
    libererArbre(arbre.racine);
    return 0;
}

