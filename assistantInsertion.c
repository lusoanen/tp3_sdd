#include "assistantInsertion.h"

/*****************************************************/
/* rechercheInsertionFrere()                         */
/*                                                   */
/* Recherche, dans la liste chainée des frères,      */
/* l'emplacement alphabétique d'une lettre.          */
/*                                                   */
/* Input : debut (double pointeur sur le début de la */
/* liste chainée des frères), lettre.                */
/* Output : pprec                                    */
/*****************************************************/

noeud_t ** rechercheInsertionFrere(noeud_t ** debut, char lettre){
    
    noeud_t ** pprec = debut;
    noeud_t * cour = *pprec;
    char lettreExamine = cour->token;

    char lettreCherche = tolower(lettre);
    lettreExamine = tolower(lettreExamine);

    while(cour != NULL && lettreExamine < lettreCherche){
        if(cour->frere == NULL || tolower(cour->frere->token) > lettreCherche){
            return pprec;
        }
        pprec = &(cour->frere);
        cour = cour->frere;
        if(cour != NULL){
            lettreExamine = cour->token;
            lettreExamine = tolower(lettreExamine);
        }
    }

    return pprec;
}

/*****************************************************/
/* rechercheInsertion()                              */
/*                                                   */
/* Recherche l'endroit dans l'arbre ou le mot doit   */
/* être inséré.                                      */
/*                                                   */
/* Input :  arbre (pointeur sur l'arbre), mot (mot à */
/* insérer), trouve (booléen indiquant si le mot est */
/* déjà dans l'arbre)                                */
/* Output : pprec (pointeur sur le pointeur de la où */
/* il faut insérer le mot)                           */
/*****************************************************/

noeud_t ** rechercheInsertion(arbre_t * arbre, char * mot, bool_t * trouve, int * i)
{
    noeud_t ** pprec = &(arbre->racine);
    noeud_t * cour = arbre->racine;
    *trouve = TRUE;
    int longueurMot = strlen(mot);

    while(cour != NULL && longueurMot > 0 && *trouve != FALSE){
        pprec = rechercheInsertionFrere(pprec, mot[*i]);
        cour = *pprec;
        if(cour != NULL){
            if(mot[*i] >= 'A' && mot[*i] <= 'Z')
            {
                if(cour->token == mot[*i])
                {
                    longueurMot = 0;
                    *trouve = TRUE;
                }
                else
                {
                    *trouve = FALSE;
                }
            }
            else
            {
                if(cour->frere == NULL && tolower(cour->token) != mot[*i]){
                    *trouve = FALSE;
                }
                else{
                    if(cour->fils == NULL && tolower(cour->token) == mot[*i]){
                        *trouve = FALSE;
                    }
                    else{
                        if(cour->frere != NULL && tolower(cour->frere->token) < mot[*i]){
                            *trouve = FALSE;
                        }
                        else{
                            pprec = &(cour->fils);
                            cour = cour->fils;
                            longueurMot--;
                            (*i)++;
                        }
                    }
                }
            }
        }
    }
    return pprec;
}

/*****************************************************/
/* motVersBonFormat()                                */
/*                                                   */
/* Transforme le mot pour qu'il corresponde au format*/
/* demander par l'arbre (toutes les lettre en        */
/* minuscule sauf celle indiquand la fin du mot)     */
/*                                                   */
/* Input :  mot (mot à transformer),                 */
/* taille (taille du mot)                            */
/* déjà dans l'arbre)                                */
/* Output :                                          */
/*****************************************************/

void motVersBonFormat(char * mot, int taille)
{
    int i = 0;
    for(i=0; i<taille; i++)
    {
        if(mot[i+1] != '\0'){
            mot[i] = tolower(mot[i]);
        }
        else{
            mot[i] = toupper(mot[i]);
        }
    }
}