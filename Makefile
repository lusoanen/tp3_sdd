#CC : le compilateur à utiliser
CC=gcc

#CFLAGS : les options de compilation  
CFLAGS= -Wall -Wextra -g

# les librairies à utiliser 
LIBS=

#LDFLAGS : les options d'édition de lien
LDFLAGS=

#lieu où se trouve les sources :
SRC=./
SRC_INC= 

#les fichiers objets
OBJ=$(SRC)main.o \
    $(SRC)liste.o \
    $(SRC)pile.o \
    $(SRC)arbre.o \
    $(SRC)assistantInsertion.o \
    $(SRC)gestionArbre.o\
    $(SRC)menu.o

tp: $(OBJ)
	$(CC) $(LDFLAGS) $^ -o $@ $(LIBS)

$(SRC)%.o: $(SRC)%.c $(SRC)%.h
	$(CC) $(CFLAGS)  $(LDFLAGS) -c $< -o $@

clean:
	rm -rf $(SRC)*.o out