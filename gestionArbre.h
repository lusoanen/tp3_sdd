#ifndef _GESTION_H_
#define _GESTION_H_

#include "arbre.h"
#include "pile.h"
#include "bool.h"
#include "noeud.h"
#include "assistantInsertion.h"

#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

void affichageMotifMot(pile_t * pileMot, char motif[]);
void afficherMots(noeud_t * depart, char motif[]);
void rechercheMotif(char motif[], arbre_t arbre);
noeud_t* rechercheDansFrere(noeud_t *debut, char lettre);
void rechercheMotif(char motif[], arbre_t arbre);
void insererMot(arbre_t * arbre, char mot[]);
noeud_t * recherchePrec(char motif[], noeud_t *depart, bool_t *trouve);
noeud_t* construireArbre(noeud_t *racine, char expression[], pile_t pile);
#endif
