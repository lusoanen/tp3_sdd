#include "pile.h"

/*****************************************************/
/* creerPile()                                       */
/*                                                   */
/* Allocation mémoire d'une pile de type             */
/* pile_t par l'appel de creerListe                  */
/*                                                   */
/* Input :                                           */
/* Output : pointeur sur l'élément de type pile_t    */
/*****************************************************/

pile_t *creerPile()
{
    pile_t *pile = creerListe();
    return pile;
}

/*****************************************************/
/* libererPile()                                     */
/*                                                   */
/* libération de l'espace mémoire alloué à la pile,   */
/* par l'appel de viderListe.                        */
/*                                                   */
/* Input :  pile (pointeur sur pile)                 */
/* Output :                                          */
/*****************************************************/

void libererPile(pile_t *pile)
{
    viderListe(pile);
    free(pile);
}

/*****************************************************/
/* empiler()                                         */
/*                                                   */
/* Insert une nouvelle valeur, passée en paramètre   */
/* dans la pile pointée, par l'appel de insererListe.*/
/*                                                   */
/* Input :  pile (pointeur sur la pile)              */
/*          valeur (valeur à inserer)                */
/* Output :                                          */
/*****************************************************/

void empiler(pile_t *pile, elem_t valeur)
{
    insererListe(&(pile->tete), valeur);
}

/*****************************************************/
/* dépiler()                                         */
/*                                                   */
/* Supprime l'élément en haut de la pile, par l'appel*/
/* de supprimerElement.                              */
/*                                                   */
/* Input :  pile (pointeur sur la pile)              */
/* Output :                                          */
/*****************************************************/

void depiler(pile_t *pile)
{
    supprimerElement(&(pile->tete));
}


/*****************************************************/
/* dépilerSansSuppression()                          */
/*                                                   */
/* Retourne l'élément en haut de la pile, et le      */
/* depile.                                           */
/*                                                   */
/* Input :  pile (pointeur sur la pile)              */
/* Output :                                          */
/*****************************************************/

elem_t depilerSansSuppression(pile_t *pile)
{
    elem_t sommet = sommetPile(pile);
    depiler(pile);
    return sommet;
}

/*****************************************************/
/* sommetPile()                                      */
/*                                                   */
/* Renvoie la valeur de la tête de pile c'est à dire */
/* le prochain élement qui sera dépilé.              */
/*                                                   */
/* Input :  pile (pointeur sur la pile)              */
/* Output : valeur de l'élément en tête de pile      */
/*****************************************************/

elem_t sommetPile(pile_t *pile)
{
    return  pile->tete->elem;
}


/*****************************************************/
/* estVide()                                         */
/*                                                   */
/* Retourne si la pile est vide (1) ou non (0) en    */
/* vérifiant si le pointeur sur la tête de liste     */
/* vaut NULL ou non.                                 */
/*                                                   */
/* Input :  pile (pointeur sur la pile)              */
/* Output :  entier de type bool_t                   */
/*****************************************************/

bool_t estVide(pile_t *pile)
{
    bool_t vide = FALSE;
    if (pile->tete == NULL)
        vide = TRUE;
    return vide;
}

/*****************************************************/
/* depilerJusquA()                                   */
/*                                                   */
/* Dépile jusqu'à l'élément courant pointer.         */
/*                                                   */
/* Input :  pile (pointeur sur la pile), courant     */
/* (pointeur sur un noeud)                           */
/* Output :                                          */
/*****************************************************/

void depilerJusquA(pile_t * pileMot, noeud_t * courant)
{
    while(sommetPile(pileMot) != courant)
    {
        depiler(pileMot);
    }

    depiler(pileMot);

}