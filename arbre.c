#include "arbre.h"

/*****************************************************/
/* creerNoeud()                                      */
/*                                                   */
/* Création d'un nouveau noeud, raccordement à       */
/* l'arbre directement                               */
/*                                                   */
/* Input :  prec (pointeur du précédent dans l'arbre)*/
/*          valeur (valeur à inserer)                */
/* Output :  pointeur sur le nouveau noeud           */
/*****************************************************/

noeud_t * creerNoeud(noeud_t **prec, char valeur){
    noeud_t *nouveau = (noeud_t*)malloc(sizeof(noeud_t));
    if(nouveau == NULL){
        fprintf (stderr, "err=%d: %s\n", errno, strerror (errno));
    }
    else {
        nouveau->token = valeur;
        nouveau->frere = NULL;
        nouveau->fils = NULL;
        if(prec != NULL)
        {
            *prec = nouveau;
        }
    }
    return nouveau;
}

/*****************************************************/
/* libererNoeud()                                    */
/*                                                   */
/* Libération du noeud en mémoire                    */
/*                                                   */
/* Input :  cour (pointeur sur le noeud à liberer)   */
/* Output :                                          */
/*****************************************************/

void libererNoeud(noeud_t *cour){
    free(cour);
}

/*****************************************************/
/* libererArbre()                                    */
/*                                                   */
/* Libération de l'arbre en mémoire                  */
/*                                                   */
/* Input :  racine (pointeur sur le premier noaud)   */
/* Output :                                          */
/*****************************************************/

void libererArbre(noeud_t *racine){
    pile_t *pile = creerPile();
    bool_t finPile = FALSE;
    noeud_t *cour = racine;
    noeud_t *depile;

    while(finPile != TRUE){
        while(cour != NULL){
            empiler(pile, cour);
            cour = cour->fils;
        }
        if (estVide(pile) == FALSE){
            depile = depilerSansSuppression(pile);
            cour = depile->frere;
            libererNoeud(depile);
        } else {
            finPile = TRUE;
        }
    }
    libererPile(pile);
    racine = NULL;

}

/*****************************************************/
/* afficherArbreDebug()                              */
/*                                                   */
/* Affichage graphique de l'arbre                    */
/*                                                   */
/* Input :  racine (pointeur sur le premier noeud)   */
/* Output :                                          */
/*****************************************************/

void afficherArbreDebug(noeud_t *racine){

    pile_t *pile = creerPile();
    bool_t fin = FALSE;
    noeud_t *cour = racine;
    int profondeur = 0;
    
    while (fin != TRUE) {
        while (cour != NULL) {
            for (int i = 0; i < profondeur; i++) {
                printf("│  ");
            }
            printf("├─── %c\n", cour->token);
            empiler(pile, cour);
            cour = cour->fils;
            profondeur++;
        }
        if (estVide(pile) != TRUE) {
            cour = depilerSansSuppression(pile);
            cour = cour->frere;
            profondeur--;
        } else {
            fin = TRUE;
        }
    }
    printf("\n");
    libererPile(pile);
}

