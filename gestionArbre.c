#include "gestionArbre.h"

/*****************************************************/
/* affichageMotifMot()                               */
/*                                                   */
/* Affichage d'un mot eventuelemnt précédé d'un motif*/
/*                                                   */
/* Input :  pileMot (poiteur sur une pile contenant  */
/* les lettre du mot), motif                         */
/* Output :                                          */
/*****************************************************/

void affichageMotifMot(pile_t * pileMot, char motif[])
{
    pile_t * reserve = creerPile();
    char lettre;

    while(!estVide(pileMot))
    {
        empiler(reserve, depilerSansSuppression(pileMot));
    }

    if(motif != NULL)
        printf("%s",motif);
    else {
        if(!estVide(reserve)){
            lettre = sommetPile(reserve)->token;
            printf("%c", toupper(lettre));
            empiler(pileMot, depilerSansSuppression(reserve));
        }
    }

    while(!estVide(reserve))
    {
        lettre = sommetPile(reserve)->token;
        printf("%c", tolower(lettre));
        empiler(pileMot, depilerSansSuppression(reserve));
    }
    printf("\n");

    libererPile(reserve);
}

/*****************************************************/
/* afficherMots()                                    */
/*                                                   */
/* Affichage des mots du dictionnaire eventuelement  */
/* précédé d'un motif                                */
/*                                                   */
/* Input :  depart (poiteur sur le noeud de départ), */
/* motif                                             */
/* Output :                                          */
/*****************************************************/

void afficherMots(noeud_t * depart, char motif[])
{
	noeud_t * courant = depart;
    pile_t * pileAffichage = creerPile();
	pile_t * pileMot = creerPile();

	while(courant != NULL)
    {

        while(courant != NULL)
		{
            if(courant->frere != NULL)
            {
                empiler(pileAffichage, courant);
            }
            
            empiler(pileMot, courant);

            if(courant->token>='A' && courant->token<='Z')
            {
                if(motif != NULL)
                    affichageMotifMot(pileMot, motif);
                else
                    affichageMotifMot(pileMot,NULL);
            }
            courant=courant->fils;

        }

		if(!estVide(pileAffichage))
        {
            courant = sommetPile(pileAffichage);
            depilerJusquA(pileMot, courant);
            courant = courant->frere;
            depiler(pileAffichage);
        }

    }

	libererPile(pileAffichage);
	libererPile(pileMot);
}

/*****************************************************/
/* rechercheDansFrere()                              */
/*                                                   */
/* Recherche d'une lettre parmis la liste chainée    */
/* des frères                                        */
/*                                                   */
/* Input :  debut (poiteur sur le noeud de départ),  */
/* lettre                                            */
/* Output : pointeur sur le noeud contenant la lettre*/
/* cherchée, null si non trouvée                     */
/*****************************************************/

noeud_t* rechercheDansFrere(noeud_t *debut, char lettre){
    noeud_t *cour = debut;

    char lettreCherche = tolower(lettre);
    char lettreExamine = tolower(cour->token);

    while(cour != NULL && lettreExamine != lettreCherche){
        cour = cour->frere;
        if(cour != NULL)
            lettreExamine = tolower(cour->token);
    }

    return cour;
}

/*****************************************************/
/* recherchePrec()                                   */
/*                                                   */
/* Recherche où se trouve la fin du motif dans       */
/* l'arbre, si elle existe                           */
/*                                                   */
/* Input :  depart (poiteur sur le noeud de départ   */
/* pour la recherche), motif, trouve (pointeur pour  */
/* confimer ou non l'existance d'une suite au motif) */
/* lettre                                            */
/* Output : pointeur sur le noeud trouvé,            */
/* null si non trouvé                                */
/*****************************************************/

noeud_t * recherchePrec(char motif[], noeud_t *depart, bool_t *trouve){
    noeud_t **pprec = &depart;
    noeud_t *cour = depart;
    *trouve = TRUE;
    int longueurMotif = strlen(motif);
    int i = 0;

    while(cour != NULL && longueurMotif > 0 && *trouve != FALSE){
        cour = rechercheDansFrere(cour, motif[i]);
        if(cour != NULL){
            pprec = &(cour->fils);
            cour = cour->fils;
            longueurMotif--;
            i++;
            if(cour == NULL && longueurMotif > 0)
                *trouve = FALSE;
        }
        else{
            *trouve = FALSE;
        }
    }
    return *pprec;
}

/*****************************************************/
/* rechercheMotif()                                  */
/*                                                   */
/* Recherche du motif dans l'arbre, et affiche les   */
/* mots composé de ce motif si il/ils existe/nt      */
/*                                                   */
/* Input :  motif, arbre                             */
/* Output :                                          */
/*****************************************************/

void rechercheMotif(char motif[], arbre_t arbre){
    bool_t motifTrouve;
    noeud_t *postMotif = recherchePrec(motif, arbre.racine, &motifTrouve);

    if(motifTrouve == TRUE){
        if(postMotif == NULL)
            printf("Ce motif %s n'a pas de suffixe dans l'arbre\n", motif);
        else{
            printf("Voici les mots composé du motif %s\n", motif);
            afficherMots(postMotif, motif);
        }
    }
    else 
        printf("Le motif %s n'existe pas dans l'arbre\n", motif);
}

/*****************************************************/
/* construireArbre()                                 */
/*                                                   */
/* Construit l'arbre en mémoire qui découle de       */
/* l'expression algébrique                           */
/*                                                   */
/* Input :  racine (poiteur d'entrée dans l'arbre),  */
/* expression, pile                                  */
/* Output : pointeur sur le premier noeud            */
/*****************************************************/

noeud_t* construireArbre(noeud_t *racine, char expression[], pile_t pile){
    
    bool_t validation = TRUE, empile = FALSE;
    int i = 0;
    noeud_t **cour = &racine;
    noeud_t *sommet;
    noeud_t *nouveau;

    while(expression[i] != '\0'){
        switch(expression[i]){
            case '(' :
                empile = TRUE;
                break;
            case ')' : 
                depiler(&pile);
                break;
            case '+' : 
                sommet = depilerSansSuppression(&pile);
                cour = &(sommet->frere);
                empile = TRUE;
                break;
            case '*' : 
                cour = &((*cour)->fils);
                break;
            default : 
                if(racine == NULL){
                    nouveau = creerNoeud(&racine, expression[i]);
                }
                else {
                    nouveau = creerNoeud(cour,expression[i]);   
                }

                if(nouveau == NULL)
                        validation = FALSE;
                if(empile == TRUE){
                    empiler(&pile, nouveau);
                    empile = FALSE;
                }
                break;
        }       
        i++;
    }
    if (validation == FALSE){
        fprintf (stderr, "Construction impossible");
    }
    
    return racine;
}

/*****************************************************/
/* insererMot()                                      */
/*                                                   */
/* Insère un mot choisi dans l'arbre                 */
/*                                                   */
/* Input :  arbre (pointeur sur l'arbre), mot        */
/* Output :                                          */
/*****************************************************/

void insererMot(arbre_t * arbre, char mot[])
{
	int i = 0;
    int taille = strlen(mot);
	motVersBonFormat(mot, taille);
	bool_t trouve = TRUE;

    noeud_t ** pprecLieu = rechercheInsertion(arbre, mot, &trouve, &i);
	noeud_t * lieuInsertion = *pprecLieu;

	if(trouve)
	{
		printf("\nCe mot est déjà présent dans l'arbre\n");
	}
    else
	{
		if(mot[i] >= 'A' && mot[i] <= 'Z'){
			lieuInsertion->token = toupper(lieuInsertion->token);
		}
		else{

			noeud_t * tete = creerNoeud(NULL, mot[i]);
			noeud_t * cour = tete;
			
			i++;
        	for(int j = i; j < taille; j++){
            	cour->fils = creerNoeud(NULL, mot[j]);
            	cour = cour->fils;
        	}
		
        	if(tete->token == tolower(lieuInsertion->token))
			{
				lieuInsertion->fils = tete->fils;
			}
			else if(tolower(lieuInsertion->token) < tete->token)
			{
				noeud_t * memoire = lieuInsertion->frere;
				lieuInsertion->frere = tete;
				lieuInsertion->frere->frere = memoire;
			}
			else
			{
				tete->frere = *pprecLieu;
				*pprecLieu = tete;
			}
		}
        printf("\nLe mot a bien été ajouté\n");
		
    }
}